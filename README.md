# pip_test_1

Simple eligibility calculator mirroring an Excel spreadsheet model, with a web-based UI

Deployed to: https://pip-test-1.herokuapp.com/

## Specification

In the [provided spreadsheet](/Bencap_calculator-PipInterviewAssignment.xlsm), the `calculator` sheet shows an example calculator. You can see it broken down into several sections. Usually these sections are in separate sheets like `enums` and `constants` as well.

Input section defines all the inputs the calculator can use to compute the value. These inputs can come from various sources, APIs, user input, data stores etc - the engine is not concerned as long as its getting the value, and in the correct format. We can potentially have hundreds of inputs coming into the engine.

Engine section defines the calculations for the engine. Each cell is a "function" which computes a final or intermediary value for another function. These cells can change over time as the policies change. We try to keep each cell as simple as possible.

Output is usually an "endpoint" calling a specific function of the engine. We use this convention to inform the engineers which function should render where in the UI. So in the `calculator` sheet there will be a label in the UI along with its computed value on the right.

There are 2 other sheets, `enums` and `constants` which are essentially constant values for engine / UI to use. These can be used several times in the engine, or serve as a control value which can be tweaked down the road without changing the engine itself. We try to avoid using static strings and numbers in the engine to maximise configurability.

### Deliverables

There are 2 main deliverables expected here.

1. A simple static UI created in HTML/CSS/JS which allows the user to set inputs and see the output configured in the `calculator` sheet. Your will be using the second deliverable in this UI. We recommend using Bootstrap to create the UI.  You may use a framework for Javascript if you like.

2. Create a simple "engine" library which can be consumed on the client side. The library will hold all the logic, enums and constants for the calculator. We should be able to call the required output functions from the library. We recommend using Javascript / Typescript for writing this library.

### Things to Consider

1. Configure any other necessary libraries (e.g. for utilities)
2. Use latest features of Javascript
3. Structure it to scale, there can be thousands of Engine functions layers over multiple "stages"
4. Write unit tests for all engine functions
5. Configure a build tool so it can be compiled into an optimized bundle to be consumed by the client (or Node backend)
6. Bonus: Use typescript
7. Bonus: Optimisations for run time performance

## Getting started

### Prerequisites

To run locally, you need to clone the repo to an environment with Ruby 2.5.1, Bundler, and Postgresql already set up. You will need to customise `config/database.yml` to your local db login, or ensure that the current user can access the db and has create database permissions. Depending on your local setup, running `bundle` in the repo may also reveal additional requirements to get everything running.

### Installing

```shell
git clone git@gitlab.com:DanielWhitston/pip_test_1.git
cd pip_test_1
bundle
rake db:setup
```

### Running locally

Following installation, ensure postgresql server is running, cd to the project root, and:

`bundle exec rails s`

Then browse to http://localhost:3000/ and you should see the application UI.

### Running tests

Following installation, ensure postgresql server is running, cd to the project root, and:

`bundle exec rspec`

## Deployment

As a standard Rails app, a lot of code hosting sites have pre-existing automated deployment options. GitLab's is messier than e.g. GitHub's, but I've set up continuous deployment to Heroku. The underlying Docker image merges the latest Ruby and Node images, as Rails 5 has dependency on a recent version of Node. I manually ran database migrations on the Heroku Postgres db, as I haven't yet set up Heroku CLI migration trigger as part of deployment. https://stackoverflow.com/questions/37514755/gitlab-ci-deploy-to-heroku-and-run-migrations may offer a model for doing this.

## Built with

* Ruby 2.5.1
* Rails 5.2.1
* Postgresql 9.5
* Bootstrap 4.1.3

## Roadmap

The intention for this initial version is to use my existing skills and knowledge to build a back-end calculator, with a simple front-end view, that ideally explores some concepts which may be useful in the longer term. I'm going to use Rails, which although extremely heavy for a project of this size, has the advantages of familiarity and extensibility. I'm also likely going to use existing gems to handle inclusion of Bootstrap and state machine handling.

- [x] ~~Consider using a state machine gem to manage calculation state and updating~~
- [x] Create a Rails project and set up the TDD / BDD framework
- [x] ~~Consider using Cucumber to define rules in readable format~~
- [x] Write an initial set of unit / integration tests
- [x] Write code to solve the tests
- [x] Check live, local operation
- [x] Create a simple front end view with a form to post calculations, and response display
- [x] Set up automated testing
- [x] Set up automated deployment and deploy the code
- [ ] Write tests for input, process and output elements
- [ ] Add support for setting input type and validation in the model and implementing in the front end
  - [ ] Date
  - [ ] Currency
  - [ ] Enum single-select
- [ ] Improve calculation 'show' display
- [ ] Consider setting up front end test framework
- [ ] Consider extending the back end, to update only changed inputs and their dependents
- [ ] Consider extending or rewriting the front end, to submit and update calculations dynamically

This road map changed somewhat during development. The traditional outside-in approach to TDD implied starting the whole thing with a front end integration test. I started with the back end first for reasons of familiarity, and also because future development would likely separate the whole thing into a separate API and front end app in any case. I also lapsed on TDD due to the amount of exploration involved in finding a workable code structure.

## Design notes

### Back end / front end

I started off with a back end calculator that accepts POST requests to a single endpoint that contain the required inputs, and returns either the required outputs, or an error if the inputs fail validation. The front end is a simple form that shows inputs, possibly validates them before sending, and then shows the results.

Longer term, the objective would be to have a front end that dynamically updates the outputs and the state of the calculation based on changing inputs. That implies a rather heavier front end with much closer ties to the back end, which is less immediately a Rails 'thing'. In particular, dynamically updating the calculation in the back end, as discussed below, *may* be relatively inefficient in Rails compared to some other options.

### Storage of calculations

#### Using Rails models and ActiveRecord

As a first version, I used a combination approach, where

* The inputs and outputs of a calculation were defined across many models
* There's only a single Calculation controller
* The controller communicates with a single Calculation model
* The Calculation model instantiates the wider structure from controller input or the database, and serializes it into a single JSON field

The intention was mainly to avoid complexity, make the storage more sensible, and keep the door open for arbitrary variation of the calculation structure without having to modify the database schema. However, the approach fits awkwardly with the wider Rails framework, and I've had to deviate from the standard structure. It took far more work than was ideal to find an approach that worked, and involved overriding ActiveRecord in places, which I would have preferred to avoid.

#### General storage notes

For short term storage, a session cookie or other session data storage are fine.

For longer term storage, a database record for a calculation makes sense. I'd be inclined to suggest using a key-value store (e.g. hstore or jsonb) to store the full calculation data in a single column of a table, with metadata such as the user ID, calculator version, created_at, updated_at and so on as separate columns. This would imply a save method that pulls together all of the inputs (and outputs?) from across the calculator, and a load method that instantiates all of the objects and their inputs.

For storage and updating of the calculation in 'real time', i.e. with dynamically updating outputs as the inputs are changed, I can see a few options:

* Storing a dynamically updating calculation in an SQL database and retrieving and updating it each time a user input changes the calculation is a pretty terrible idea, in terms of both scalability and responsiveness
* Storing calculation data in session cache, then re-instantiating and re-performing the full calculation in the back end every time a change gets sent across, is also not ideal. If the calculation exceeds 4K, it will also require extra setup to ensure that it doesn't get stored in the SQL database anyway, and instead gets stored in a Redis instance or localStorage or some such
* A pure front-end calculation would obviate this issue, but would of course expose the full calculation codebase to end users
* A framework and / or language that either persists sessions and data while a calculation is ongoing, or reinstantiates them with very low latency, would go a long way to resolving this in the longer term

### Calculation code structure

As a first pass, a calculation should be a collection of objects representing the different elements of the calculation, each with their own rules but with enforced and standardised input and output methods, and with dependencies across the calculation managed using dependency injection.

I'm not convinced state machines in the sense of e.g. [AASM](https://github.com/aasm/aasm) are useful immediately, as state is entirely defined by the inputs rather than being persisted regardless. Instead, making all calculation elements inherit from a base model, with a standard set of specs applied to each (plus custom specs for desired behaviour and validations), can be used to verify that the entire calculation chain adheres to standard behaviours.

The idea is that by taking an OOP approach to the design, the calculation can be made arbitrarily complex without embedding too much complexity in a single level or element. Making data available across the calculation using dependency injection may complicate this goal, if there's ever any logic which relies on inputs or state from several layers up, across, and down again in the object hierarchy.

It may be useful to consider versioning calculations *within* the code base, in order to enable retrieval of saved calculations from prior versions. This would possibly open the door to conversion and comparison of calculations across multiple calculators, and to a single application running multiple calculators at once. That would have major effects on the code structure, and on the routing / request handling.

#### Inputs

Each input object:

* keeps the object in memory
* sets the object from JSON / string
* outputs the object in format suitable for calculations
* outputs the object in form suitable for embedding in saved JSON
* validates the object, and returns an error if invalid
* sets a default value if one is not provided

I'm saving all inputs as a single field in the database, partly to avoid changing the structure all the time, and partly for ease of access. Given that, using ActiveRecord models or attributes for individual inputs doesn't make much sense. The only model attributes with full access to ActiveRecord functionality are the ones being saved.

I've ended up using ActiveModel as a representation. That also is incomplete. ActiveModel doesn't support attributes on Rails 5, so e.g. attribute defaults aren't really a thing.

## Versioning

There isn't an API mode or a separate front-end yet, so this is largely academic. If developed further, it would make sense to use SemVer.

## Authors

Written by [Daniel Whitston](https://github.com/danwhitston).

## License

This project uses an [MIT License](/LICENSE).
