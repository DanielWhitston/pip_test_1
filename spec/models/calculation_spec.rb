require 'rails_helper'

RSpec.describe Calculation, type: :model do
  let(:valid_attributes) {
    { calculation: {
      input_user_age: '01/01/2014', input_housing_sector: 'Shared Ownership',
      input_owner_eligible: 'Yes', input_housing_owner_loan: 'Yes', input_owner_capital: 30000,
      input_owner_service: 1500 } }
  }

  let(:invalid_attributes) {
    {calculation: {input_housing_sector: 'this will not work'} }
  }

  # validations
  it 'is valid with valid attributes' do
    calculation = Calculation.new(valid_attributes)
    expect(calculation).to be_valid
  end

  it 'is not valid without a calculation' do
    calculation = Calculation.new(invalid_attributes)
    expect(calculation).to_not be_valid
  end
end
