require 'rails_helper'

RSpec.describe "calculations/index", type: :view do
  before(:each) do
    assign(:calculations, [
      Calculation.create!(
        :calculation => { input_user_age: '01/01/2014', input_housing_sector: 'Shared Ownership',
          input_owner_eligible: 'Yes', input_housing_owner_loan: 'Yes', input_owner_capital: 30000,
          input_owner_service: 1500 }
      ),
      Calculation.create!(
        :calculation => { input_user_age: '01/01/2012', input_housing_sector: 'Shared Ownership',
          input_owner_eligible: 'Yes', input_housing_owner_loan: 'Yes', input_owner_capital: 10000,
          input_owner_service: 1500 }
      )
    ])
  end

  it "renders a list of calculations" do
    render
    assert_select "tr>td", :html => /input_user_age/, :count => 2
  end
end
