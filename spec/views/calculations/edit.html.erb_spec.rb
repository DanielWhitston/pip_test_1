require 'rails_helper'

RSpec.describe "calculations/edit", type: :view do
  before(:each) do
    @calculation = assign(:calculation, Calculation.create!(
      :calculation => { input_user_age: '01/01/2014', input_housing_sector: 'Shared Ownership',
        input_owner_eligible: 'Yes', input_housing_owner_loan: 'Yes', input_owner_capital: 30000,
        input_owner_service: 1500 }
    ))
  end

  it "renders the edit calculation form" do
    render

    assert_select "form[action=?][method=?]", calculation_path(@calculation), "post" do

      assert_select "input[name=?]", "calculation[calculation[input_housing_owner_loan]]"
    end
  end
end
