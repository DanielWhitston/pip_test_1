require 'rails_helper'

RSpec.describe "calculations/show", type: :view do
  before(:each) do
    @calculation = assign(:calculation, Calculation.create!(
      :calculation => { input_user_age: '01/01/2014', input_housing_sector: 'Shared Ownership',
        input_owner_eligible: 'Yes', input_housing_owner_loan: 'Yes', input_owner_capital: 30000,
        input_owner_service: 1500 }
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
  end
end
