require 'rails_helper'

RSpec.describe "Calculations", type: :request do
  describe "GET /calculations" do
    it "responds successfully" do
      get calculations_path
      expect(response).to have_http_status(200)
    end
  end
end
