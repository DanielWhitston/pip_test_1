class Calculation < ApplicationRecord
  validate :calculation, :validate_inputs

  after_initialize do
    # Populate the various fields prior to displaying them
    @inputs = InputList.new(Calculation::INPUT_FIELDS)
    @inputs.set_values(self.calculation&.fetch('inputs', nil)&.symbolize_keys)
    @outputs = set_outputs
  end

  # Match input keys to their classes
  # This structure won't play nicely with multiple copies of an input, e.g. for two different people
  # However, it should be possible to support that with minor changes. Or with multiple input names :-(
  INPUT_FIELDS = {
    input_user_age: Input::UserAge,
    input_housing_sector: Input::HousingSector,
    input_owner_eligible: Input::OwnerEligible,
    input_housing_owner_loan: Input::HousingOwnerLoan,
    input_owner_capital: Input::OwnerCapital,
    input_owner_service: Input::OwnerService
  }

  # Match engine logic steps to their classes
  # Not currently used! All connectors are hardcoded. Frankly everything could be hardcoded.
  # PROCESSING_STEPS = {
  #   s2_dwp_owner_eligible: Process::S2DwpOwnerEligible,
  #   s2_dwp_owner_mortgage: Process::S2DwpOwnerMortgage,
  #   s2_dwp_owner_mortgage_limit: Process::S2DwpOwnerMortgageLimit,
  #   s2_dwp_owner_standardinterest: Process::S2DwpOwnerStandardInterest,
  #   s1_pensioner_user: Process::S1PensionerUser
  # }

  # Define output fields and match to classes
  OUTPUT_FIELDS = {
    output_owner_occupier: Output::OwnerOccupier,
    output_owner_interest: Output::OwnerInterest
  }

  def calculation=(input_hash)
    @inputs = InputList.new(INPUT_FIELDS, input_hash)
    @outputs = set_outputs
    super({
      inputs: inputs.serialized,
      outputs: serialize_outputs
    })
  end

  def self.permitted_inputs
    INPUT_FIELDS.keys
  end

  def inputs
    @inputs ||= InputList.new(INPUT_FIELDS)
  end

  def validate_inputs
    inputs.add_validation_errors(errors)
  end

  def set_outputs
    OUTPUT_FIELDS.map do |output, klass|
      klass.new(inputs)
    end
  end

  def serialize_outputs
    @outputs.map{|output| output.as_json}.inject(:merge)
  end
end
