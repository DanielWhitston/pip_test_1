class InputList
  attr_accessor :list

  # Returns an array of input objects matching the ones given against those available
  def initialize(input_klasses, input_hash = {})
    @list = {}
    input_klasses.each do |input_type, klass|
      @list[input_type] = klass.new(input_hash[input_type])
    end
  end

  def set_values(retrieved_inputs = nil)
    return unless retrieved_inputs.present?
    @list.each do |input_type, input_object|
      if retrieved_inputs[input_type].present?
        input_object.value = retrieved_inputs[input_type]
      end
    end
  end

  def serialized
    @list.values.map(&:as_json).reduce(&:merge)
  end

  def add_validation_errors(calculation_errors)
    @list.values.each do |input|
      calculation_errors.merge!(input.errors) if input.invalid?
    end
  end
end
