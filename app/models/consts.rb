class Consts
  def self.dwp_owner_mortgage_limit_pensionage
    100000
  end

  def self.dwp_owner_mortgage_limit_workingage
    200000
  end

  def self.dwp_owner_mortgage_interest_percent
    0.261
  end
end
