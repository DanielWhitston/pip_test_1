class Input::OwnerEligible < Input::ApplicationInput

  validates_presence_of :input_owner_eligible
  validates :input_owner_eligible, inclusion: { in: %w(Yes No) }

  attr_accessor :input_owner_eligible

  def initialize(input_value = nil)
    @input_owner_eligible = input_value || default
  end

  def default
    'Yes'
  end

  def value
    @input_owner_eligible
  end

  def value= new_value
    @input_owner_eligible = new_value
  end

  def type
    :radio
  end

  def options
    ['Yes', 'No']
  end

  def label
    'Can you claim help with mortgage costs?'
  end

  def attributes
    { 'input_owner_eligible' => nil }
  end
end
