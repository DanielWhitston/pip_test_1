# Stores input_owner_service. Assumption of positive integer, i.e. pounds no pennies
class Input::OwnerService < Input::ApplicationInput

  validates_presence_of :input_owner_service
  validate :input_owner_service, :is_positive_currency?

  attr_accessor :input_owner_service

  def initialize(input_value = nil)
    input_value ||= default
    @input_owner_service = input_value.to_i
  end

  def default
    0
  end

  def value
    @input_owner_service
  end

  def value= new_value
    @input_owner_service = new_value.to_i
  end

  def type
    :currency
  end

  def label
    'Owner occupier communal service charge'
  end

  def attributes
    {
      'input_owner_service' => nil
    }
  end

  def is_positive_currency?
    @input_owner_service.is_a?(Integer) && @input_owner_service >= 0
  end

end
