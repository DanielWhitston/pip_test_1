# An input to enter the housing sector that a user belongs to
class Input::HousingSector < Input::ApplicationInput

  ENUMS_TENURE = {
    enums_tenure_nohousingcosts: 'No housing costs',
    enums_tenure_private: 'Rented from a private landlord',
    enums_tenure_owner_occupier: 'Owner Occupier',
    enums_tenure_sharedownership: 'Shared Ownership'
  }

  validates_presence_of :input_housing_sector
  validates :input_housing_sector, inclusion: { in: ENUMS_TENURE.keys }

  attr_accessor :input_housing_sector

  def initialize(input_value = nil)
    input_value ||= default
    @input_housing_sector = ENUMS_TENURE.key(input_value) || input_value
  end

  def default
    ENUMS_TENURE[:enums_tenure_nohousingcosts]
  end

  def value
    ENUMS_TENURE[@input_housing_sector]
  end

  def value= new_value
    @input_housing_sector = new_value.to_sym
  end

  def type
    :radio
  end

  def options
    ENUMS_TENURE.values
  end

  def label
    'What is your housing sector?'
  end

  def attributes
    {
      'input_housing_sector' => nil
    }
  end
end
