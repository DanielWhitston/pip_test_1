# Stores input_owner_capital. Assumption of positive integer, i.e. pounds no pennies
class Input::OwnerCapital < Input::ApplicationInput

  validates_presence_of :input_owner_capital
  validate :input_owner_capital, :is_positive_currency?

  attr_accessor :input_owner_capital

  def initialize(input_value = nil)
    input_value ||= default
    @input_owner_capital = input_value.to_i
  end

  def default
    0
  end

  def value
    @input_owner_capital
  end

  def value= new_value
    @input_owner_capital = new_value.to_i
  end

  def type
    :currency
  end

  def label
    'Owner occupier capital outstanding'
  end

  def attributes
    {
      'input_owner_capital' => nil
    }
  end

  def is_positive_currency?
    @input_owner_capital.is_a?(Integer) && @input_owner_capital >= 0
  end

end
