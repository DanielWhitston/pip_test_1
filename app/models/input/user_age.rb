# An input to enter a users age
# Per the spreadsheet, this has no restrictions on dates in the future or distant past
class Input::UserAge < Input::ApplicationInput

  validates_presence_of :input_user_age
  validate :input_user_age, :is_a_date?

  attr_accessor :input_user_age

  def initialize(input_value = nil)
    @input_user_age = Date.parse(input_value.presence || default) # Locale appears to have taken effect?
  end

  def default
    Date.today.strftime("%d/%m/%Y")
  end

  def value
    @input_user_age.strftime("%Y-%m-%d")
  end

  def value= new_value
    @input_user_age = Date.parse(new_value)
  end

  def type
    :date
  end

  def label
    'Age of User'
  end

  def attributes
    {
      'input_user_age' => nil
    }
  end

  def is_a_date?
    @input_user_age.is_a? Date
  end

end
