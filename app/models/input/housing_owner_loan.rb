# Whether there's a housing owner loan or not
class Input::HousingOwnerLoan < Input::ApplicationInput

  validates_presence_of :input_housing_owner_loan
  validates :input_housing_owner_loan, inclusion: { in: %w(Yes No) }

  attr_accessor :input_housing_owner_loan

  def initialize(input_value = nil)
    @input_housing_owner_loan = input_value || default
  end

  def default
    'Yes'
  end

  def value
    @input_housing_owner_loan
  end

  def value= new_value
    @input_housing_owner_loan = new_value
  end

  def type
    :radio
  end

  def options
    ['Yes', 'No']
  end

  def label
    'Will you take out a DWP loan to assist with mortgage interest?'
  end

  def attributes
    {
      'input_housing_owner_loan' => nil
    }
  end
end
