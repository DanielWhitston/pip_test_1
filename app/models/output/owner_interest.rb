class Output::OwnerInterest < Output::ApplicationOutput
  attr_accessor :output_owner_interest

  def initialize(input_list)
    input_owner_capital = input_list.list[:input_owner_capital]
    input_user_age = input_list.list[:input_user_age]
    @output_owner_interest = Process::S2DwpOwnerStandardInterest.with_inputs(input_owner_capital, input_user_age)
  end

  def attributes
    {
      output_owner_interest: nil
    }
  end
end
