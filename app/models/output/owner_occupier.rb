class Output::OwnerOccupier < Output::ApplicationOutput
  attr_accessor :output_owner_occupier

  def initialize(input_list)
    input_housing_sector = input_list.list[:input_housing_sector]
    input_owner_eligible = input_list.list[:input_owner_eligible]
    @output_owner_occupier = Process::S2DwpOwnerEligible.with_inputs(input_housing_sector, input_owner_eligible)
  end

  def attributes
    {
      output_owner_occupier: nil
    }
  end
end
