class Enums
  # This is grim on multiple levels. First, Ruby doesn't really support enumerators
  # in the way that's intended here. Second, there's nothing in the structure that enforces
  # the underlying concept of an object with a limited set of uniquely named states. Third,
  # there's no performance advantage to this implementation.

  def self.tenure_nohousingcosts
    'No housing costs'
  end

  def self.tenure_private
    'Rented from a private landlord'
  end

  def self.tenure_owner_occupier
    'Owner Occupier'
  end

  def self.tenure_sharedownership
    'Shared Ownership'
  end

  def self.yes
    'Yes'
  end

  def self.no
    'No'
  end
end
