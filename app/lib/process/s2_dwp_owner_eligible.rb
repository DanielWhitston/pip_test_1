class Process::S2DwpOwnerEligible < Process::ApplicationProcess
  def self.with_inputs(input_housing_sector, input_owner_eligible)
    if (input_housing_sector.input_housing_sector == Enums.tenure_owner_occupier || input_housing_sector.input_housing_sector == Enums.tenure_sharedownership) && (input_owner_eligible.input_owner_eligible == Enums.yes)
      Enums.yes
    else
      Enums.no
    end
  end
end
