class Process::S2DwpOwnerStandardInterest < Process::ApplicationProcess
  def self.with_inputs(input_owner_capital, input_user_age)
    s2_dwp_owner_mortgage_limit = Process::S2DwpOwnerMortgageLimit.with_inputs(input_user_age)
    s2_dwp_owner_mortgage = Process::S2DwpOwnerMortgage.with_inputs(input_owner_capital)
    if s2_dwp_owner_mortgage > s2_dwp_owner_mortgage_limit
      (s2_dwp_owner_mortgage_limit * Consts.dwp_owner_mortgage_interest_percent)/12
    else
      (s2_dwp_owner_mortgage * Consts.dwp_owner_mortgage_interest_percent)/12
    end
  end
end
