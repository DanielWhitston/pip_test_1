class Process::S2DwpOwnerMortgageLimit < Process::ApplicationProcess
  def self.with_inputs(input_user_age)
    s1_pensioner_user = Process::S1PensionerUser.with_inputs(input_user_age)
    if s1_pensioner_user == Enums.yes
      Consts.dwp_owner_mortgage_limit_pensionage
    else
      Consts.dwp_owner_mortgage_limit_workingage
    end
  end
end
