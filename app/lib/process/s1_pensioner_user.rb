class Process::S1PensionerUser < Process::ApplicationProcess
  def self.with_inputs(input_user_age)
    if input_user_age.input_user_age > Date.new(1953, 12, 16)
      Enums.yes
    else
      Enums.no
    end
  end
end
