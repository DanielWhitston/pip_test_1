class Process::S2DwpOwnerMortgage < Process::ApplicationProcess
  def self.with_inputs(input_owner_capital)
    input_owner_capital.input_owner_capital
  end
end
