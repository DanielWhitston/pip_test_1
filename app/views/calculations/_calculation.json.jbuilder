json.extract! calculation, :id, :calculation, :created_at, :updated_at
json.url calculation_url(calculation, format: :json)
